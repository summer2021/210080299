# weekly work report
https://github.com/AOSC-Dev/OSPP2021-LoongArch64-Port
See also week*_report.md of this repo for details

# interim report document

https://docs.google.com/document/d/1gq9jqApS7uV9i_2TuL5i5cpjcxHBX4eX81wa45wj-O0/edit?usp=sharing
It has been uploaded to the OSPP background management system before August 16

# Closing report document

https://docs.google.com/document/d/1ceb2gzN8EQPB6fSr1OJ0GBtv4_XXqOUxl9tj3quQLQI/edit?usp=sharing
It was uploaded to the OSPP background management system on September 2

# Porting flowchart

https://drive.google.com/file/d/1wvofKLUDevp3DdjXAbwCY-IFtJ8cmQ3z/view?usp=sharing

#CLFS-for-LoongArch

# CLFS made by me

## Old CLFS

Utilizes Loongnix's GCC-8 toolchain and built according to CLFS_For_LoongArch64-20210618. At present, it has been used in the AOSC OS basic packaging environment. Due to sensitive reasons such as source code ownership and vector instructions, it is not convenient to disclose.

## New CLFS

The new CLFS I made is based on [GCC-12 LoongArch64 port](https://github.com/loongson/gcc/tree/loongarch-12), due to the modification of the open source tool chain and the compatibility between the open source kernel and the Longmeng motherboard, temporarily unable to test on the actual machine. For details, see [clfs-git-new.tar.gz](https://gitlab.summer-ospp.ac.cn/summer2021/210080299/-/blob/main/clfs-git-new.tar.gz)

## Loongson Sun Haiyong's CLFS

I made some compilation parameters and compatibility modifications on the CLFS tutorial of Mr. Sun Haiyong. For details, see [CLFS] of this repo (https://gitlab.summer-ospp.ac.cn/summer2021/210080299/-/blob/main/CLFS_For_LoongArch64-20210618.md)

How to cross compile an LFS (Linux From Scratch) system based on the LoongArch architecture. [CLFS_For_LoongArch64-20210801](https://github.com/sunhaiyong1978/CLFS-for-LoongArch) from Loongson Sun Haiyong

[CLFS_For_LoongArch64-20210801](https://github.com/sunhaiyong1978/CLFS-for-LoongArch/blob/main/CLFS_For_LoongArch64-20210801.md):

Corresponding CLFS system: [loongarch64-clfs-system-20210801](https://github.com/sunhaiyong1978/CLFS-for-LoongArch/releases/download/20210801/loongarch64-clfs-system-20210801.tar.bz2)

Cross tool chain: [loongarch64-clfs-20210801-cross-tools](https://github.com/sunhaiyong1978/CLFS-for-LoongArch/releases/download/20210801/loongarch64-clfs-20210801-cross-tools.tar. xz)

# package results

Completed the packaging of all deb packages in AOSC OS core-* base-* x11-*. As of September 28, 772 deb packages have been produced and installed on CLFS.

# Porting software

Personal porting are written entirely by myself or by an AOSC member.
Loongson porting refers to the source code or patch originating from Loongson Corporation. I made some minor modifications in the upstream git libffi and made it public. For the convenience of porting, it is mentioned here.

## Personal porting

- dpkg see [loongarch64-dpkg.patch](https://gitlab.summer-ospp.ac.cn/summer2021/210080299/-/blob/main/loongarch64-dpkg.patch)
- libbsd see [libbsd-add-loongarch64.patch.loongarch64](https://gitlab.summer-ospp.ac.cn/summer2021/210080299/-/blob/main/loongarch64-dpkg.patch)
- autobuild3 see https://github.com/AOSC-Dev/autobuild3/blob/master/arch/loongarch64.sh

## Loongson porting

- GCC-12 https://github.com/loongson/gcc/tree/loongarch-12
- Glibc https://github.com/loongson/glibc/tree/loongarch_2_33
- binutils https://github.com/loongson/binutils-gdb/tree/loongarch-2_37
- Linux kernel https://github.com/loongson/linux/tree/loongarch-next
- grub2 https://github.com/loongarch64/grub/tree/dev-la64
- libffi https://github.com/xyq1113723547/libffi
- systemd see the patches folder for details
- automake see the patches folder for details

## Show pictures
See the images folder of this repo for details
